import { browser, element, by } from 'protractor';

export class LampertSolutionsPage {
	navigateTo() {
		return browser.get ( '/' );
	}

	getParagraphText() {
		return element ( by.css ( 'app-root h1' ) ).getText ();
	}

	getAllText() {
		return element ( by.css ( 'app-root' ) ).getText ();
	}
}
