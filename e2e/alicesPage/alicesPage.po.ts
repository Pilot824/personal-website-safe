/**
 * Created by andrew on 04/04/17.
 */
import { browser, element, by } from 'protractor';

export class AlicesE2EPage {
	navigateTo() {
		return browser.get('/#/alice');
	}

	getAllText(){
		return element(by.css('app-alice')).getText();
	}
}
