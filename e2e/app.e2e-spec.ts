import { LampertSolutionsPage } from './app.po';

describe ( 'lampert-solutions App', () => {
	let page : LampertSolutionsPage;

	beforeAll ( () => {
		spyOn ( console, 'error' );

		page = new LampertSolutionsPage ();
		page.navigateTo ();
	} );

	it ( 'should contain a header with Lampert.Solutions', () => {
		expect ( page.getParagraphText () ).toContain ( 'Lampert.Solutions' );
	} );

	it ( 'should contain a header with Andrew Lampert', () => {
		expect ( page.getParagraphText () ).toContain ( 'Andrew Lampert' );
	} );

	it ( 'error should not be called', () => {
		expect ( console.error ).not.toHaveBeenCalled ();
	} );

	it ( 'should not contain profanities', function () {
		expect ( page.getAllText() ).not.toContain("fuck");
		expect ( page.getAllText() ).not.toContain("shit");
		expect ( page.getAllText() ).not.toContain("damn");
	} );
} );
