<?php

include("db_fns.php");


if( isset($_GET['query']) ){

	$query = "";

	// Preset Queries
	if( $_GET['query'] == 'resume_companies' ){

		$query = "SELECT
				company.company_id,
				company.title,
				company.description,
				company.main_location
			FROM company";

		if( isset($_GET['company_id']) ){

			$query .= " WHERE company_id = " . $_GET['company_id'];

		}
	} else if( $_GET['query'] == 'resume_positions' ){

		$query = "SELECT
			positions.position_id,
			positions.company_id,
			positions.title,
			positions.description,
			positions.positionType,
			positions.prefix,
			positions.suffix,
			positions.branch_id,
			positions.icon,
            positions.showInResume,
            positions.show
		FROM positions
		WHERE positions.show = 1";

	} else if( $_GET['query'] == 'resume_skills' ){

		$query = "SELECT DISTINCT
			skills.skill_id,
			skills.name,
			skills.description,
			skills.category
		FROM skills_positions_map
		LEFT JOIN skills ON skills_positions_map.skill_id = skills.skill_id
		WHERE skills_positions_map.position_id = " . $_GET['position_id'];

	} else if( $_GET['query'] == 'titles' ){

		$query = "SELECT
			GROUP_CONCAT(prefix SEPARATOR ', ') AS prefix,
			GROUP_CONCAT(suffix SEPARATOR ', ') AS suffix
		FROM positions
		WHERE hasTitle = 1";

	} else if ( $_GET['query'] == 'titles_full'){

		$query = "SELECT title FROM `positions` WHERE hasTitle = 1";


	} else if ( $_GET['query'] == 'skill_categories' ){

		$query = "SELECT DISTINCT(category) FROM skills";

	} else if ( $_GET['query'] == 'resume_terms' ){

		if( isset($_GET['position_id']) ){

			$query = "SELECT
			    term_id, position_id, start_date, end_date
			FROM position_terms
			WHERE position_id = " . $_GET['position_id'] . " ORDER BY
			start_date, end_date";

		}

	} else if ($_GET['query'] == 'wishlist_getall'){

		$query = "SELECT
			wishlist.wishlist_id,
			wishlist.title,
			wishlist.start_date,
			wishlist.end_date,
			wishlist.received,
			wishlist.where_location,
			wishlist.where_url,
			wishlist.cost,
			wishlist.priority
			FROM wishlist
			ORDER BY wishlist.priority ASC, end_date DESC, cost ASC";

	} else if ($_GET['query'] == 'notifs_getall'){

		$query = "SELECT
			notifications.notification_id,
			notifications.text,
			notifications.start_date,
			notifications.end_date,
			notifications.version,
			notifications.type,
			TRUE AS visible
			FROM notifications
            WHERE (
				notifications.start_date IS NULL
                OR notifications.start_date <= NOW()
            ) AND (
				notifications.end_date IS NULL
                OR notifications.end_date >= NOW()
            )
			ORDER BY notifications.end_date DESC,
            notifications.version DESC";
	}

	$data = db_query($query);

} else if (isset($_GET['action'])){

	if( $_GET['action'] == 'set_concat_len'){
		db_query("SET SESSION group_concat_max_len = 15000;");
	}

} else if (isset($_GET['query_spec'])){

	if ($_GET['query_spec'] == 'resume_getall'){

		$query = "SELECT
                  		positions.position_id,
                  		positions.title AS position_title,
                  		positions.description,
                  		positions.positionType,
                  		(
                  			SELECT
                  				CONCAT(
                  					'[',
                  					GROUP_CONCAT(
                  						CONCAT_WS(
                  							'',
                  							'{ \"start_date\" : \"',
                  							COALESCE(position_terms.start_date, ''),
                  							'\"\, \"end_date\" : \"',
                  							COALESCE(position_terms.end_date, ''),
                  							'\"}'
                  						)
                  						SEPARATOR ', '
                  					),
                  					']'
                  				)
                  			FROM position_terms
                  			WHERE position_terms.position_id = positions.position_id
                  		) AS terms_json,
                  		(
                  			SELECT
                  				CONCAT(
                  					'[',
                  					GROUP_CONCAT(
                  						CONCAT_WS(
                  							'',
                  							'{ \"skill_name\" : \"',
                  							COALESCE(skills.name, 'NULL'),
                  							'\"\, \"category\" : \"',
                  							COALESCE(skills.category, 'NULL'),
                  							'\"}'
                  						)
                  						SEPARATOR ', '
                  					),
                  					']'
                  				)
                  			FROM skills_positions_map
                  			LEFT JOIN skills on skills.skill_id = skills_positions_map.skill_id
                  			WHERE skills_positions_map.position_id = positions.position_id
                  		) AS skills_json,
                  		company.title AS company_title,
                  		company.main_location AS company_location,
                  		company_branch.title AS branch_title,
                  		company_branch.location AS branch_location,
                  		positions.icon,
                  		positions.showInResume,
                  		@@group_concat_max_len
                  		FROM positions
                  		LEFT JOIN company ON company.company_id = positions.company_id
                  		LEFT JOIN company_branch ON positions.branch_id = company_branch.branch_id
                          LEFT JOIN position_terms ON positions.position_id = position_terms.position_id
                  		WHERE positions.show = 1
                        GROUP BY positions.position_id
                  		ORDER BY position_terms.end_date IS NULL DESC, position_terms.end_date DESC" ;



		$sql_conn = db_connection();

		$sql_conn->query("SET @@group_concat_max_len = 1024*1024*1024;");

		$data = db_build_res($sql_conn, $query);

		// $data = db_query($query);

		$sql_conn->close();

	}
} else {
	$data = array("Error", "No Data");
}

echo(json_encode($data));
