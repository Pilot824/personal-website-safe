<?php

if( isset($_GET['url']) ){

	getRSSFeed( $_GET['url'] );

}


function getRSSFeed($rssurl){
	$baseURL = "https://api.rss2json.com/v1/api.json?rss_url=";

	$json = file_get_contents($baseURL . $rssurl);
	$jdecoded = json_decode($json, true);

	echo(json_encode($jdecoded));

}
