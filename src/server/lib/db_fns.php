<?php

header('Access-Control-Allow-Origin: *');

function db_connection(){

	$accessLocal = false;
	if($_SERVER["SERVER_NAME"] == "localhost") $accessLocal = true;

	if( $accessLocal ){
		$dbServer		= "localhost";
		$dbAccountDB	= "lamperttechnologies";
	} else {
		$dbServer		= "lampert.ipowermysql.com";
		$dbAccountDB	= "lamperttechnologies";
	}

	$sql = new mysqli($dbServer, $dbAccountUser, $dbAccountPass, $dbAccountDB);

	return $sql;
}

function db_build_res($mysqli, $q){

	$returnArray = array();

	if ($mysqli->connect_errno) {
		// The connection failed. What do you want to do?
		// You could contact yourself (email?), log the error, show a nice page, etc.
		// You do not want to reveal sensitive information

		// Let's try this:
		echo "Sorry, this website is experiencing problems.";

		// Something you should not do on a public site, but this example will show you
		// anyways, is print out MySQL error related information -- you might log this
		echo "Error: Failed to make a MySQL connection, here is why: \n";
		echo "Errno: " . $mysqli->connect_errno . "\n";
		echo "Error: " . $mysqli->connect_error . "\n";

		// You might want to show them something nice, but we will simply exit
		exit;
	}

	// if( strpos($q, "SET SESSION group_concat_max_len") == 0){

	// 	$mysqli->query($q);
	// 	echo("Calling query");

	// } else
	if (!$result = $mysqli->query($q)) {
		// Oh no! The query failed.
		echo "Sorry, the website is experiencing problems.";

		// Again, do not do this on a public site, but we'll show you how
		// to get the error information
		echo "Error: Our query failed to execute and here is why: \n";
		echo "Query: " . $q . "\n";
		echo "Errno: " . $mysqli->errno . "\n";
		echo "Error: " . $mysqli->error . "\n";
		exit;
	}

	if($result !== true){

		while ($row = $result->fetch_assoc()) {
			array_push($returnArray, $row);
		}

		$result->free();
	}

	return($returnArray);
}


function db_query($q){


	$mysqli = db_connection();

	return db_build_res($mysqli, $q);

	$mysqli->close();


}
