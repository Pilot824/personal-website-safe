<?php

include('../db_fns.php');

// FUNCTIONS
function getCourses($courseId) {
	$query = "SELECT
		`training_courses`.`courseId` AS `courseId`,
		`training_courses`.`name` AS `name`,
		`training_courses`.`branchId` AS `branchId`,
		`training_courses`.`description` AS `description`,
		`training_courses`.`startDate` AS `startDate`,
		`training_courses`.`endDate` AS `endDate`,
		`training_courses`.`hiddenBeforeStart` AS `hiddenBeforeStart`,
		`company_branch`.`title` AS `organization`
		from (`training_courses`
		left join `company_branch` on((`training_courses`.`branchId` = `company_branch`.`branch_id`)))
		where (`training_courses`.`hidden` = 0)";

	if($courseId){
		$query .= " AND training_courses.courseId = " . $courseId;
	}

	return  db_query($query);
}

function getCourseModules($courseId){
	$query = "SELECT * FROM training_module WHERE courseId = "  . $courseId;

	return  db_query($query);
}

function getCourseContent($moduleId){
	$query = "SELECT * FROM training_content WHERE moduleId = "  . $moduleId;

	return  db_query($query);
}
