<?php

include('resume_fns.php');

// Get & filter GET call

$GETPARAM = $_GET;
$POSTPARAM = $_POST;

// TODO: FILTERING

$data = array("error", "data was not set");

if(isset($GETPARAM['action'])){
	if($GETPARAM['action'] == 'getSkills'){
		$data = getSkills();
	} else if($GETPARAM['action'] == 'getWork'){
		$data = getPositions('Work');
	} else if($GETPARAM['action'] == 'getEducation'){
		$data = getPositions('Education');
	} else if($GETPARAM['action'] == 'getExtra'){
		$data = getPositions('Extra');
	} else if($GETPARAM['action'] == 'getProjects'){
		$data = getPositions('Project');
	} else if($GETPARAM['action'] == 'getAwards'){
		$data = getPositions('Award');
	} else if($GETPARAM['action'] == 'getCompanyBranches'){
		$data = getCompanyBranches();
	} else if($GETPARAM['action'] == 'getLanguageProgression'){
		$data = getLanguageProgression();
	} else if ($GETPARAM['action'] == 'getPositionSkills'){
		$data = getPositionSkills($GETPARAM['positionId']);
	}
} else {
	$data = array("error", "No Data");
}

echo(json_encode($data));
