<?php

include('../db_fns.php');

// FUNCTIONS

function getSkills() {
	$query = "SELECT
		skills.name,
		skills.description,
		skills.category,
		skills.showInResume
	FROM skills_positions_map
    LEFT JOIN skills ON skills.skill_id = skills_positions_map.skill_id
	GROUP BY skills_positions_map.skill_id";

	return  db_query($query);
}

function getPositions($positionType) {
	$query = "SELECT
		positions.position_id as id,
		positions.title as name,
		positions.description,
		positions.showInResume,
		positions.positionType,
		positions.branch_id,
        GROUP_CONCAT(
			position_terms.start_date
		) AS start_dates,
        GROUP_CONCAT(
			position_terms.end_date
		) AS end_dates,
		IF(hasTitle, prefix, '') AS prefix,
		IF(hasTitle, suffix, '') AS suffix
	FROM positions
	LEFT JOIN position_terms ON positions.position_id = position_terms.position_id
	WHERE positions.positionType = '" . $positionType . "'
	AND positions.show = 1
    GROUP BY positions.position_id
    ORDER BY positions.position_id";

	return  db_query($query);
}

function getCompanyBranches(){
	$query = "SELECT
		company.company_id AS companyId,
		company_branch.branch_id AS id,
		company.title AS companyTitle,
		company.description AS companyDescription,
		company.main_location AS companyLocation,
		company_branch.title AS companyBranchTitle,
		company_branch.location AS companyBranchLocation
	FROM `company_branch`
	LEFT JOIN company ON company_branch.company_id = company.company_id";

	return  db_query($query);
}

function getLanguageProgression(){
	$query = "SELECT
		skills.skill_id AS id,
		skills.name,
		position_terms.position_id AS positionId,
		position_terms.start_date AS startDate
	FROM `skills_positions_map`
	LEFT JOIN skills ON skills.skill_id = skills_positions_map.skill_id
	LEFT JOIN position_terms ON position_terms.position_id = skills_positions_map.position_id
	WHERE skills.category = 'Programming Languages'
	AND start_date IS NOT NULL
	ORDER BY `position_terms`.`start_date` ASC";

	return  db_query($query);
}

function getPositionSkills($positionId){
	$query = "SELECT
			skills.name,
			skills.category,
			skills.showInResume
		FROM skills
		LEFT JOIN skills_positions_map ON skills.skill_id = skills_positions_map.skill_id
		WHERE skills_positions_map.position_id = " .$positionId. "
		GROUP BY skills.skill_id;";

	return  db_query($query);
}
