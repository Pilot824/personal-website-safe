import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../../../environments/environment';
import { GroundSchoolComponent } from '../../pages/training/ground-school/ground-school.component';
import { GroundSchoolUserModel } from '../../models/ground-school-user.model';
import { training_module, training_courses, training_coursesFields, training_coursesWithCompany, training_content, training_contentFields } from '../../models/training.model';
import { CourseModel, ModuleModel } from '../../models/course.model';

@Injectable()
export class TrainingService {

	private groundSchoolURL = `${environment.serverBaseURL}/server/lib/groundschool/groundschool.php`;
	private trainingURL = `${environment.serverBaseURL}/server/lib/training/training.php`;

	constructor(private http: Http) {

	}

	// TODO: Rewrite to be generic

	getGSUsers(): Observable<GroundSchoolUserModel[]> {
		return this.http.get(`${this.groundSchoolURL}?action=getUsers`).map(data => data.json());
	}

	getGSUserTestAttempts(memberId: number): Observable<any> {
		return this.http.get(`${this.groundSchoolURL}?action=getUserTestAttempts&member_id=${memberId}`).map(data => data.json());
	}

	getTests(): Observable<any> {
		return this.http.get(`${this.groundSchoolURL}?action=getTests`).map(data => data.json());
	}

	getCourses(): Observable<training_coursesWithCompany[]> {
		return this.http.get(`${this.trainingURL}?action=getCourses`).map(data => data.json());
	}

	getCourseById(courseId: training_coursesFields.courseId): Observable<training_coursesWithCompany> {
		return this.http.get(`${this.trainingURL}?action=getCourses&courseId=${courseId}`).map(data => data.json()[0]);
	}

	getCourseModules(courseId: training_coursesFields.courseId): Observable<training_module[]> {
		return this.http.get(`${this.trainingURL}?action=getCourseModules&courseId=${courseId}`).map(data => data.json());
	}

	getCourseContent(moduleId: training_contentFields.moduleId) {
		return this.http.get(`${this.trainingURL}?action=getCourseContent&moduleId=${moduleId}`).map(data => data.json());
	}

	getCourseWithContent(courseId: training_coursesFields.courseId): Observable<CourseModel> {
		return this.getCourseById(courseId).switchMap(dbCourse => {

			return this.getCourseModules(courseId).switchMap((dbCourseModules: training_module[]) => {

				let filteredModules: training_module[] = dbCourseModules.filter((courseModuleFilter: training_module) => {
					// Return flase if the obj is not set
					if (!courseModuleFilter) {
						return false;
					}

					if ((<any>courseModuleFilter.hidden) == 1) {
						return false;
					}

					// Do not filter if start date isn't set
					if (!!courseModuleFilter && !courseModuleFilter.startDate) {
						return true;
					}

					let startDateVal = new Date(`${courseModuleFilter.startDate} EST`).valueOf();
					let currentDateVal = Date.now().valueOf();

					// Fixing boolean
					courseModuleFilter.hiddenBeforeStart = (<any>courseModuleFilter.hiddenBeforeStart) != 0;

					let showModule = !courseModuleFilter.hiddenBeforeStart || (currentDateVal >= startDateVal);

					return showModule;
					// return !courseModuleFilter.hiddenBeforeStart || (currentDateVal >= startDateVal);
				});

				if (filteredModules.length == 0) {
					return new Observable(subscriber => {
						subscriber.next([]);
						subscriber.complete();
					});
				}

				dbCourseModules = filteredModules;

				return Observable.combineLatest(dbCourseModules.map((dbCourseModule: training_module) => {

					return this.getCourseContent(dbCourseModule.moduleId).map((dbCourseContent: training_content[]) => {
						return {
							...dbCourseModule,
							content: dbCourseContent.map(content => {
								content.href = this.hrefSolver(content.href, dbCourse.courseId);
								return content;
							})
						};
					});

				}));
			}).map((courseModules: ModuleModel[]) => {
				return {
					...dbCourse,
					hidden: false,
					modules: courseModules
				};
			});

		}).map((courses: CourseModel) => {
			return courses;
		});
	}

	hrefSolver(originalHref: string, courseId: number | string): string {

		let httpStr = 'http://';

		if (!originalHref || originalHref.substr(0, httpStr.length) == httpStr) {

			return originalHref;

		} else if (!!originalHref && !!courseId) {

			return `${environment.serverBaseURL}/assets/files/${courseId}/${originalHref}`;

		}

		return originalHref;
	}

}
