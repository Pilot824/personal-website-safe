import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaseService } from '../base/base.service';
import { ToastyService } from 'ng2-toasty';
import { HTTPQUERYTYPE } from '../../globals';
import { environment } from '../../../environments/environment';

@Injectable()
export class ServerService extends BaseService {

	protected serverPath: string;

	constructor(
		protected http: Http,
		protected toastyService: ToastyService
	) {
		super(toastyService);
	}

	getAction(path: string = this.serverPath, queryMode: HTTPQUERYTYPE, action = '') {
		if (queryMode == HTTPQUERYTYPE.GET) {
			if (!!(action)) {
				let url = `${environment.serverBaseURL}/server/lib/${path}?action=${action}`;
				return this.http
					.get(url)
					.map(response => response.json() as any[])
					.catch(this.handleError);
			}
		}
	}
}
