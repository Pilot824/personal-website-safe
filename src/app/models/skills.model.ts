export class SkillsModel {
	name: string;
	category: string;
	showInResume: boolean;
}
