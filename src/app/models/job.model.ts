import { BaseModel } from './base.model';
import { SkillsModel } from './skills.model';
import { JOBTYPE } from '../globals';
import { BranchModel } from './branch.model';

export class JobModel extends BaseModel {
	name: string;
	description: string;
	type: JOBTYPE;
	terms: {
		start: Date,
		end?: Date
	}[];
	skills?: SkillsModel[];
	title: {
		full: string;
		short?: string;
	};
	showInResume: boolean;
	companyBranch?: BranchModel;
}
