import { BaseModel } from './base.model';

export class NavigationElementModel extends BaseModel {

	name: string;
	href: string;

	zmdiIcon?: string;
	subNavElements?: NavigationElementModel[];

	disabled?: boolean;

}
