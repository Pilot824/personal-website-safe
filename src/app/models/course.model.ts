import { BaseModel } from './base.model';
import { training_courses, training_coursesFields, training_module, training_moduleFields, training_content } from './training.model';

export class CourseModel implements training_courses {
	courseId: training_coursesFields.courseId;
	branchId: training_coursesFields.branchId;
	name: training_coursesFields.name;
	description: training_coursesFields.description;
	startDate: training_coursesFields.startDate;
	endDate: training_coursesFields.endDate;
	hidden: training_coursesFields.hidden;
	hiddenBeforeStart: training_coursesFields.hiddenBeforeStart;

	modules: ModuleModel[];

}

export class ModuleModel implements training_module {
	moduleId: training_moduleFields.moduleId;
	courseId: training_moduleFields.courseId;
	name: training_moduleFields.name;
	description: training_moduleFields.description;
	startDate: training_moduleFields.startDate;
	hidden: training_moduleFields.hidden;
	hiddenBeforeStart: training_moduleFields.hiddenBeforeStart;

	content: training_content[];
}
