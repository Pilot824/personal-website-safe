import { BaseModel } from './base.model';

export class LectureModel extends BaseModel {
	dateStr?: string;
	date: Date;
	description?: string;
	content: { text: string, href?: string }[];
}
