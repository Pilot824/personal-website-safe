import { BaseModel } from './base.model';

export class BranchModel extends BaseModel {
	companyId: number;
	companyTitle: string;
	companyDescription: string;
	companyLocation: string;
	companyBranchTitle: string;
	companyBranchLocation: string;
}
