import { Component, OnInit } from '@angular/core';
import { TrainingService } from '../../services/training/training.service';
import { training_courses, training_coursesFields, training_coursesWithCompanyFields, training_coursesWithCompany } from '../../models/training.model';

@Component({
	selector: 'app-training',
	templateUrl: 'training.component.html'
})

export class TrainingComponent implements OnInit {

	public trainingCourses: training_coursesWithCompany[] = [];

	constructor(
		private trainingService: TrainingService
	) { }

	ngOnInit() {

		this.getTrainingCourses().subscribe(courses => {
			this.trainingCourses = courses;
		});
	}

	getTrainingCourses() {
		return this.trainingService.getCourses();
	}

}
