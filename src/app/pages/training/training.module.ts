import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { TrainingRoutingModule } from './training.routing';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatCardModule, MatToolbarModule, MatButtonModule, MatExpansionModule } from '@angular/material';

import { TrainingComponent } from './training.component';
import { GroundSchoolComponent } from './ground-school/ground-school.component';
import { CourseComponent } from './course/course.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { PanelModule, ChartModule } from 'primeng/primeng';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,

		SharedModule,

		TrainingRoutingModule,

		MatToolbarModule,
		MatButtonModule,
		MatCardModule,
		MatExpansionModule,

		PanelModule
	],
	exports: [],
	declarations: [
		TrainingComponent,
		GroundSchoolComponent,
		CourseComponent,
		StatisticsComponent
	],
	providers: [],
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA
	]
})
export class TrainingModule { }
