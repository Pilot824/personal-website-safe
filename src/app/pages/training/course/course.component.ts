import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TrainingService } from '../../../services/training/training.service';
import { training_courses, training_module, training_coursesWithCompany } from '../../../models/training.model';
import { Observable } from 'rxjs/Observable';
import { CourseModel } from '../../../models/course.model';

@Component({
	selector: 'app-course-component',
	templateUrl: 'course.component.html'
})

export class CourseComponent implements OnInit {

	private courseId: number;
	public course: CourseModel;
	public modules: training_module[];

	constructor(
		private route: ActivatedRoute,
		private trainingService: TrainingService
	) { }

	ngOnInit() {

		this.courseId = this.route.snapshot.params['courseId'];

		this.trainingService.getCourseWithContent(this.courseId).subscribe(course => {
			this.course = course;
		});

	}


}
