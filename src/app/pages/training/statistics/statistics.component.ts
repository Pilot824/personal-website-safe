import { Component, OnInit } from '@angular/core';
import { TrainingService } from '../../../services/training/training.service';
import { GroundSchoolUserModel } from '../../../models/ground-school-user.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/switchMap';

@Component({
	selector: 'app-course-statistics',
	templateUrl: 'statistics.component.html'
})

export class StatisticsComponent implements OnInit {

	public users: GroundSchoolUserModel[];
	public tests: any[];
	public testAttempts: any[];
	public userTestScores: any[];

	constructor(
		private courseService: TrainingService
	) {

	}

	ngOnInit() {
		Observable.forkJoin(
			this.courseService.getGSUsers(),
			this.courseService.getTests()
		).switchMap(data => {
			[this.users, this.tests] = data;
			return this.getUserTests();
		}).subscribe(testResults => {
			this.users.forEach(user => {
				user.testAttempts = testResults.filter(testSet => {
					return testSet[0].member_id == user.member_id;
				})[0];
			});

			this.users.map(user => {

				user.averageTestScore = 0;

				user.testAttempts.map(testAttempt => {
					let test = this.tests.find(test => {
						return test.test_id == testAttempt.test_id;
					});
					if (!!test) {
						Object.keys(test).forEach(testKey => {
							testAttempt[testKey] = test[testKey];
						});
						if (testAttempt.retest == '1' && testAttempt.points > (testAttempt.total_points / 2)) {
							testAttempt.points = (testAttempt.total_points / 2);
						}
					}
					return testAttempt;
				});

				user.testAttempts.forEach(testAttempt => {
					user.averageTestScore += testAttempt.points / testAttempt.total_points;
				});

				user.averageTestScore = user.averageTestScore / user.testAttempts.length;

				return user;
			});

			this.users.sort((a, b) => {
				if (a.averageTestScore > b.averageTestScore) {
					return -1;
				} else if (a.averageTestScore < b.averageTestScore) {
					return 1;
				} else {
					return 0;
				}
			});
		});
	}

	getUserTests(): Observable<any[]> {
		if (!!(this.users)) {

			let testQueryArray = [];

			testQueryArray = this.users.map(user => {
				return this.courseService.getGSUserTestAttempts(user.member_id);
			});

			return Observable.forkJoin(testQueryArray).map(tests => this.testAttempts = tests);
		}
	}

}
