import { Component, OnInit, Input } from '@angular/core';
import { SkillsModel } from '../../../../models/skills.model';
import { SimpleChanges } from '@angular/core';
import { RESUMEMODE } from '../../../../globals';

@Component({
	selector: 'app-resume-quick-skills',
	templateUrl: 'quick-skills.component.html'
})

export class QuickSkillsComponent implements OnInit {

	@Input()
	public skills: SkillsModel[];
	categories: string[];
	RESUMEMODE = RESUMEMODE;
	@Input() public mode: RESUMEMODE = RESUMEMODE.CV;

	constructor() { }

	ngOnInit() {
		this.categories = this.getSkillCategories(this.skills);
	}

	ngOnChanges(changes: SimpleChanges) {
		this.categories = this.getSkillCategories(this.skills);

	}

	getSkillCategories(skills: SkillsModel[]): string[] {
		let categories = [];

		skills.forEach(skill => {
			if (categories.findIndex(category => category == skill.category) == -1) {
				categories.push(skill.category);
			}
		});

		return categories;
	}

	getSkillsByCategory(category: string) {
		return this.skills.filter(skill => skill.category == category);
	}
}
