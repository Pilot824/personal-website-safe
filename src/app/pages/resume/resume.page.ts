import { Component, OnInit } from '@angular/core';
import { JobModel } from '../../models/job.model';
import { SkillsModel } from '../../models/skills.model';
import { Observable } from 'rxjs/Rx';
import { RESUMEMODE, JOBTYPE } from '../../globals';
import { ResumeService } from '../../services/resume/resume.service';
import { ToastyService } from 'ng2-toasty';
import { environment } from '../../../environments/environment';

@Component({
	selector: 'app-resume-page',
	templateUrl: 'resume.page.html',
	styleUrls: ['resume.page.scss']
})

export class ResumePage implements OnInit {

	// Lists
	public jobs: JobModel[];
	public education: JobModel[];
	public awards: JobModel[];
	public projects: JobModel[];
	public extra: JobModel[];
	public skills: SkillsModel[];
	public titles: string[];

	// Page Config
	public pageTitle: string;
	public showCoverLetter: boolean;
	private cvTitle = 'CV';
	private resumeTitle = 'Resume';
	public mode: RESUMEMODE;
	public showContactInformation: boolean;

	// Imports
	public RESUMEMODE = RESUMEMODE;
	public environment = environment;

	constructor(
		private resumeService: ResumeService,
		private toastyService: ToastyService
	) {
		this.jobs = [];
		this.education = [];
		this.awards = [];
		this.skills = [];
		this.projects = [];
		this.titles = [];
		this.extra = [];
		this.mode = RESUMEMODE.CV;

		this.pageTitle = `Andrew Lampert`;
		this.showCoverLetter = true;
		this.showContactInformation = false;
	}

	ngOnInit() {
		this.showContactInformation = this.setContactInformationDisplay();

		Observable.combineLatest(
			this.resumeService.getSkills(),
			this.resumeService.getWork(),
			this.resumeService.getEducation(),
			this.resumeService.getAwards(),
			this.resumeService.getBranches(),
			this.resumeService.getProjects(),
			this.resumeService.getExtra()
		).subscribe((data: any) => {
			try {

				if (!!data) {
					// Error Checking
					for (let i = 0; i < data.length; i++) {
						let dataObj = data[i];
						if (typeof dataObj[0] == 'string' && (<any>dataObj[0]) == 'error') {
							throw new Error((<any>dataObj[1]));
						}
					}

					let branches = data[4];


					this.skills = data[0];

					this.jobs = data[1].map(position => {
						return this.resumeService.cleanJob(position, branches);
					});

					this.education = data[2].map(position => {
						return this.resumeService.cleanJob(position, branches);
					});

					this.awards = data[3].map(position => {
						return this.resumeService.cleanJob(position, branches);
					});

					this.projects = data[5].map(position => {
						return this.resumeService.cleanJob(position, branches);
					});

					this.extra = data[6].map(position => {
						return this.resumeService.cleanJob(position, branches);
					});

					// Sorting jobs and education

					this.jobs = this.resumeService.sortJobs(this.jobs);

					this.education = this.resumeService.sortJobs(this.education);

					this.awards = this.resumeService.sortJobs(this.awards);

					this.projects = this.resumeService.sortJobs(this.projects);

					this.extra = this.resumeService.sortJobs(this.extra);

					this.setPageTitle();

					this.setTitles([].concat(this.jobs, this.education, this.awards, this.projects, this.extra));
				}

			} catch (e) {
				this.toastyService.error('An Error Occured!');
				console.error(`Server Error: ${e}`);
				console.error(e);
			}

		});
	}

	parseSkillsFromJobs(jobs: JobModel[]): SkillsModel[] {
		let skills: SkillsModel[] = [];

		jobs.forEach(job => {
			skills = skills.concat(job.skills);
		});

		return skills;
	}

	parseFullTitlesFromJobs(jobs: JobModel[]): string[] {

		let titles: string[] = [];

		jobs.forEach(job => {
			if (job && job.title && job.title.full) {
				titles = titles.concat(job.title.full);
			}
		});

		return titles;
	}

	setMode(newMode: RESUMEMODE) {
		this.mode = newMode;

		// this.showCoverLetter = this.mode == RESUMEMODE.CV;
		this.showCoverLetter = true;
	}

	setPageTitle() {
		this.pageTitle = `Andrew Lampert`;
	}

	setTitles(positions: JobModel[]) {
		// positions.forEach(position => {
		// 	if (position.title) {
		// 		console.log(position);
		// 	}
		// });
	}

	setContactInformationDisplay() {
		return environment.isLocal;
	}
}
