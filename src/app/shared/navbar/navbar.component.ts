import { Component, OnInit } from '@angular/core';
import { SITENAME } from '../../globals';
import { NavigationElementModel } from '../../models/navigation-element.model';
import { NavigationService } from '../../services/navigation/navigation.service';

@Component({
	selector: 'navbar-component',
	templateUrl: 'navbar.component.html',
	styleUrls: ['navbar.component.scss']
})

export class NavbarComponent implements OnInit {

	public SITENAME = SITENAME;
	public navigationElements: NavigationElementModel[];

	constructor(
		private navigationService: NavigationService
	) { }

	ngOnInit() {
		this.navigationElements = this.navigationService.navigationList;
	}
}
