import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

import { NavbarComponent } from './navbar/navbar.component';

/* Prime NG */
import { PanelModule } from 'primeng/primeng';
import { MenuModule, TabMenuModule, MenuItem } from 'primeng/primeng';
import { CommonModule } from '@angular/common';

/* Angular Material*/
import { MatToolbarModule, MatButtonModule, MatCardModule, MatMenuModule } from '@angular/material';

/* Services */
import { ServerService } from '../services/server/server.service';
import { NavigationService } from '../services/navigation/navigation.service';
import { ResumeService } from '../services/resume/resume.service';
import { RouterModule } from '@angular/router';
import { ToastyModule } from 'ng2-toasty';
import { TrainingService } from '../services/training/training.service';
import { WishlistService } from '../services/wishlist/wishlist.service';


const sharedComponents = [
	NavbarComponent,
];

const sharedProviders = [
	ServerService,
	NavigationService,
	ResumeService,
	TrainingService,
	WishlistService
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule,

		MatToolbarModule,
		MatButtonModule,
		MatCardModule,
		MatMenuModule,

		ToastyModule.forRoot()
	],
	exports: [
		sharedComponents,
		MatButtonModule
	],
	declarations: [
		sharedComponents
	],
	providers: [
		sharedProviders
	],
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA
	]
})
export class SharedModule {
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: SharedModule,
			providers: [
				sharedProviders
			]
		};
	}
}
